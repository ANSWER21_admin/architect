package com.topstack.common.http.api

import com.topstack.hilibrary.restful.HiCall
import com.topstack.hilibrary.restful.annotation.Filed
import com.topstack.hilibrary.restful.annotation.POST

interface AccountApi {

    @POST("user/login")
    fun login(
        @Filed("userName") userName: String,
        @Filed("password") password: String
    ): HiCall<String>


    @POST("user/registration")
    fun register(
        @Filed("userName") userName: String,
        @Filed("password") password: String,
        @Filed("imoocId") imoocId:
        String, @Filed("orderId") orderId: String
    ): HiCall<String>

}