package com.topstack.aspro.biz

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import com.alibaba.android.arouter.facade.annotation.Route
import com.alibaba.android.arouter.launcher.ARouter
import com.topstack.aspro.databinding.ActivityLoginBinding
import com.topstack.common.http.ApiFactory
import com.topstack.common.http.api.AccountApi
import com.topstack.common.ui.component.HiBaseActivity
import com.topstack.common.utils.SPUtil
import com.topstack.hilibrary.restful.HiCallback
import com.topstack.hilibrary.restful.HiResponse


@Route(path = "/account/login")
class LoginActivity : HiBaseActivity() {

    private val REQUEST_CODE_REGISTRATION: Int = 1000


    private lateinit var binding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.actionBack.setOnClickListener {
            onBackPressed()
        }
        binding.actionRegister.setOnClickListener {
            goRegistration()
        }
        binding.actionLogin.setOnClickListener {
            goLogin()
        }
    }

    private fun goRegistration() {
        ARouter.getInstance().build("/account/registration")
            .navigation(this, REQUEST_CODE_REGISTRATION)
    }

    private fun goLogin() {
        val name = binding.inputItemUserName.getEditText().text
        val password = binding.inputItemUserPassword.getEditText().text
        if (TextUtils.isEmpty(name) or (TextUtils.isEmpty(password))) {
            return
        }
        ApiFactory.create(AccountApi::class.java).login(name.toString(), password.toString())
            .enqueue(object : HiCallback<String> {
                override fun onSuccess(response: HiResponse<String>) {
                    if (response.code == HiResponse.SUCCESS) {
                        showToast("登录成功")

                        val data = response.data
                        SPUtil.putString("boarding-pass", data!!)
                        setResult(Activity.RESULT_OK, Intent())
                        finish()
                    } else {
                        showToast("登录失败" + response.msg)
                    }
                }

            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((requestCode == RESULT_OK) and (data != null) and (requestCode == REQUEST_CODE_REGISTRATION)) {
            val username = data!!.getStringExtra("username")
            if (username != null) {
                binding.inputItemUserName.getEditText().setText(username)
            }
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }
}