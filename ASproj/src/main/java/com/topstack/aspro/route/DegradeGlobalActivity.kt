package com.topstack.aspro.route

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.alibaba.android.arouter.facade.annotation.Autowired
import com.alibaba.android.arouter.facade.annotation.Route
import com.topstack.aspro.R
import com.topstack.aspro.databinding.LayoutGlobalDegradeBinding
import com.topstack.common.route.HiRoute

/**
 * 全局统一错误页
 */
@Route(path = "/degrade/global/activity")
class DegradeGlobalActivity : AppCompatActivity() {
    @JvmField
    @Autowired
    var degrade_title: String? = null

    @JvmField
    @Autowired
    var degrade_desc: String? = null

    @JvmField
    @Autowired
    var degrade_action: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //ARouter.getInstance().inject(this)
        HiRoute.inject(this)
        val binding = LayoutGlobalDegradeBinding.inflate(layoutInflater)
        setContentView(R.layout.layout_global_degrade)
        binding.emptyView.setIcon(R.string.if_empty)

        if (degrade_title != null) {
            binding.emptyView.setTitle(degrade_title!!)
        }

        if (degrade_desc != null) {
            binding.emptyView.setDesc(degrade_desc!!)
        }

        if (degrade_action != null) {
            binding.emptyView.setHelpAction(listener = View.OnClickListener {
                var intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(degrade_action))
                startActivity(intent)
            })
        }
        binding.actionBack.setOnClickListener { onBackPressed() }
    }
}