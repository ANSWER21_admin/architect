package com.topstack.hilibrary.restful

import java.io.IOException

interface HiCall<T> {

    /**
     * 同步
     */
    @Throws(IOException::class)
    fun execute(): HiResponse<T>

    /**
     * 异步
     */
    fun enqueue(callback: HiCallback<T>)


    interface Factory {

        fun newCall(request: HiRequest): HiCall<*>
    }
}