package com.topstack.hiconfig.core

interface JsonParser {
    fun <T> fromJson(json: String, clazz: Class<T>): T?
}