package com.topstack.hiconfig.core

interface ConfigListener {
    fun onConfigUpdate(configMap: Map<String, Any>)
}